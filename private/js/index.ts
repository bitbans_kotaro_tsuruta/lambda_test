function get_awsDB() {
    console.log('Data読み込み中');
    var result = document.getElementById('result');
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState === 4){ // 通信完了
            if (xhr.status === 200) { // 通信成功
                var data = xhr.response;
                console.log(data.Items.length);
                if (data.Items.length === 0) {
                    console.log('データが存在しません。');
                    document.getElementById('aws_counter').innerHTML = '0';
                } else {
                    document.getElementById('aws_counter').innerHTML = data.Items[0].taskname;
                }
            } else {
                console.log('サーバエラーが発生');
            }
        } else {
            console.log('通信中');
        }
    　　};
        xhr.responseType = 'json';
        xhr.open('GET', 'https://9hlnwfewhi.execute-api.ap-northeast-1.amazonaws.com/stageTest', true);
        xhr.send(null);
}(get_awsDB());

document.addEventListener('DOMContentLoaded', function(){
    document.getElementById('submit').addEventListener('click',function(){
        var status = document.getElementById('status');
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4){
                if (xhr.status === 200) {
                    console.log('送信しました');
                } else {
                    console.log('サーバーエラーが発生しました');
                }
            } else {
                console.log('送信中');
            }
        };
        var obj = {
            Item : {
                id: 'count',
                taskname: Number(document.getElementById('aws_counter').innerHTML) + 1
            }
        };
        var json = JSON.stringify(obj);
        xhr.open('PUT', 'https://9hlnwfewhi.execute-api.ap-northeast-1.amazonaws.com/stageTest', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(json);
    });

    document.getElementById('reset').addEventListener('click',function(){
        var status = document.getElementById('status');
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4){
                if (xhr.status === 200) {
                    console.log('リセットしました。');
                } else {
                    console.log('サーバーエラーが発生しました');
                }
            } else {
                console.log('リセット中');
            }
        };
        var obj = {
            Item : {
                id: 'count',
                taskname: 0
            }
        };
        var json = JSON.stringify(obj);
        xhr.open('PUT', 'https://9hlnwfewhi.execute-api.ap-northeast-1.amazonaws.com/stageTest', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        console.log(json);
        xhr.send(json);
    });

    document.getElementById('delete').addEventListener('click',function(){
        var status = document.getElementById('status');
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4){
                if (xhr.status === 200) {
                    console.log('削除しました。');
                } else {
                    console.log('サーバーエラーが発生しました');
                }
            } else {
                console.log('削除中');
            }
        };
        var obj = {
            Item : {
                id: 'count'
            }
        };
        var json = JSON.stringify(obj);
        xhr.open('DELETE', 'https://9hlnwfewhi.execute-api.ap-northeast-1.amazonaws.com/stageTest', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        console.log(json);
        xhr.send(json);
    });
});