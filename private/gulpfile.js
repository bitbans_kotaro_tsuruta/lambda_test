const gulp = require('gulp');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const imageminPngquant = require('imagemin-pngquant');
const imageminZopfli = require('imagemin-zopfli');
const imageminGiflossy = require('imagemin-giflossy');
const cleanCSS = require('gulp-clean-css');
const browserify = require('browserify');
const source  = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const tsify = require('tsify');
const browserSync = require('browser-sync');


gulp.task('javascript', () => {
		browserify()
        .add('./js/index.ts')
        .plugin('tsify', {
            target: 'ES5',
            removeComments: true
        })
        .bundle()
				.on('error', console.error.bind(console))
        .pipe(source('app.js'))
				.pipe(buffer())
    		.pipe(uglify())
        .pipe(gulp.dest('../js/'));
});

// Static server
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "../"
        },
				port: 8100,
				ui: {
				    port: 8081
				}
    });
});

// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('sass', function () {
	gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(postcss([ autoprefixer({
			grid: true
		})]))
    .pipe(gulp.dest('../css/'));
});

gulp.task('imagemin', function(){
    gulp.src('./img/*.*')
        .pipe(imagemin([
            imageminPngquant({
                speed: 1,
                quality: 90
            }),
            imageminZopfli({
                more: true
            }),
            imageminGiflossy({
                optimizationLevel: 3,
                optimize: 3,
                lossy: 2
            }),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            imagemin.jpegtran({
				quality: 90,
                progressive: true
            })
        ]))
        .pipe(gulp.dest('../img/'));
});

gulp.task('watch', function(){
    gulp.watch('./sass/*.scss', ['sass']);
		gulp.watch('./js/*.ts', ['javascript']);
		gulp.watch(['./*.html'], ['html']);
		gulp.watch("./*.html", ['bs-reload']);
});

gulp.task('default', ['browser-sync','javascript','sass','watch']);
